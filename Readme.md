Programming interview exercises:

1. Create a linked list
2. From a string that can have repeated characters, create a hash / dictionary in which each key is the character and the value is the number of times the key appears in the string.
3. Write a function bishop_move that takes 2 arguments start and end, which are the starting and ending place of the bishop, and returns 1 if it can move to the ending place with one move and 2 if it can move in 2 moves, otherwise return null (or something convenient for the programming language you’re using) if it cannot reach the ending position. Consider a board arrangement as follows:

    1,  2,  3,  4,  5,  6,  7,  8
    9, 10, 11, 12, 13, 14, 15, 16
                            …  64


    A call to the function would be something like:

    bishop_move(1, 64)  -> returns 1
    bishop_move(1, 3) ->  returns 2
    bishop_move(1, 2)  -> returns none, some exception, etc
